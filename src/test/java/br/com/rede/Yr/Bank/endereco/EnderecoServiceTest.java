package br.com.rede.Yr.Bank.endereco;

import br.com.rede.Yr.Bank.exceptions.ContaDuplicatedException;
import br.com.rede.Yr.Bank.exceptions.EnderecoNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

@SpringBootTest
public class EnderecoServiceTest {
    @Autowired
    private EnderecoService enderecoService;

    @MockBean
    private EnderecoRepository enderecoRepository;

    @Test
    public void testarMetodoBuscarEnderecoPorIdCaminhoPositivo(){
        Endereco endereco = new Endereco();
        Optional<Endereco> enderecoOptional = Optional.of(endereco);
        Mockito.when(enderecoRepository.findById(Mockito.anyInt())).thenReturn(enderecoOptional);

        Assertions.assertEquals(endereco,enderecoService.buscarEnderecoPorId(1));
    }

    @Test
    public void testarMetodoBuscarEnderecoPorIdCaminhoNegativo(){
        Optional<Endereco> enderecoOptional = Optional.empty();
        Mockito.when(enderecoRepository.findById(Mockito.anyInt())).thenReturn(enderecoOptional);

        EnderecoNotFoundException exception = Assertions
                .assertThrows(EnderecoNotFoundException.class, () -> {enderecoService.buscarEnderecoPorId(1);});
        Assertions.assertEquals("Nenhum endereço encontrado!", exception.getMessage());
    }

    @Test
    public void testarMetodoAtualizarEndereco(){
        Endereco endereco = new Endereco();
        Optional<Endereco> enderecoOptional = Optional.of(endereco);
        Mockito.when(enderecoRepository.findById(Mockito.anyInt())).thenReturn(enderecoOptional);
        Mockito.when(enderecoRepository.save(Mockito.any(Endereco.class))).thenReturn(endereco);
        Assertions.assertEquals(endereco,enderecoService.atualizarEndereco(1,endereco));
    }

}
