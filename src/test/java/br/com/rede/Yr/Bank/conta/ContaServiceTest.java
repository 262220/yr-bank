package br.com.rede.Yr.Bank.conta;

import br.com.rede.Yr.Bank.enuns.TipoTransacao;
import br.com.rede.Yr.Bank.exceptions.*;
import br.com.rede.Yr.Bank.operacao.Operacao;
import br.com.rede.Yr.Bank.operacao.OperacaoRepository;
import br.com.rede.Yr.Bank.usuario.Usuario;
import br.com.rede.Yr.Bank.usuario.UsuarioService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class ContaServiceTest {
    @Autowired
    private ContaService contaService;

    @MockBean
    private ContaRepository contaRepository;
    @MockBean
    private OperacaoRepository operacaoRepository;
    @MockBean
    private UsuarioService usuarioService;

    private Conta conta;
    private Usuario usuario;
    private Operacao operacao;
    private Optional<Conta> contaOptional;

    @BeforeEach
    public void setUp() {
        conta = new Conta();
        usuario = new Usuario();
        usuario.setEmail("vini@gmail.com");
        usuario.setCpf("12345678910");
        conta.setAgencia("0001");
        conta.setNumConta("00001");
        conta.setSaldo(100.0);
        conta.setDigito(7);
        conta.setAtiva(true);
        conta.setTitular(usuario);
        usuario.setConta(conta);
        contaOptional = Optional.of(conta);

        operacao = new Operacao();

    }

    @Test
    public void testCriarConta() {
        Mockito.when(usuarioService.buscarUsuarioPorEmail("vini@gmail.com")).thenReturn(usuario);

        Mockito.when(usuarioService.possuiConta("12345678910")).thenReturn(true);

        Mockito.when(contaRepository.save(Mockito.any(Conta.class))).thenReturn(conta);

        Assertions.assertEquals(conta, contaService.criarConta(usuario.getEmail()));
    }

    @Test
    public void testQuantidadeContas() {
        List<Conta> contas = new ArrayList<>();
        contas.add(conta);

        Mockito.when(contaRepository.findAll()).thenReturn(contas);

        assertEquals(1, contaService.quantidadeContas());
    }

    @Test
    public void testBuscarContaPorEmailCaminhoPositivo() {
        Mockito.when(usuarioService.buscarUsuarioPorEmail("vini@gmail.com")).thenReturn(usuario);

        Mockito.when(usuarioService.possuiConta(usuario)).thenReturn(true);

        assertEquals(conta, contaService.buscarContaPorEmail(usuario.getEmail()));
    }

    @Test
    public void testBuscarContaPorNumeroCaminhoPositivo() {
        Mockito.when(contaRepository.findById("00001")).thenReturn(contaOptional);

        assertEquals(contaOptional.get(), contaService.buscarContaPorNumero("00001"));
    }

    @Test
    public void testBuscarContaPorNumeroCaminhoNegativo() {
        Mockito.when(contaRepository.findById("00001")).thenReturn(contaOptional);

        ContaNotFoundException exception =
                assertThrows(ContaNotFoundException.class, () -> {
                    contaService.buscarContaPorNumero("00010");
                });

        assertEquals(true, exception.getMessage().equals("Conta não existe!"));
    }

    @Test
    public void testAtualizarConta() {
        Mockito.when(contaRepository.save(conta)).thenReturn(conta);

        Assertions.assertEquals(conta, contaService.atualizarConta(conta));
    }

    @Test
    public void testSaldoSuficienteCaminhoPositivo() {
        Assertions.assertTrue(contaService.saldoSuficiente(50, conta.getSaldo()));
    }

    @Test
    public void testSaldoSuficienteCaminhoNegativo() {
        SaldoNotEnoughException exception =
                Assertions.assertThrows(SaldoNotEnoughException.class, () -> {
                    contaService.saldoSuficiente(200, conta.getSaldo());
                });

        Assertions.assertEquals(exception.getMessage(), "Saldo insuficiente!");
    }

    @Test
    public void testContaAtivaCaminhoPositivo() {
        Assertions.assertTrue(contaService.contaAtiva(conta));
    }

    @Test
    public void testContaAtivaCaminhoNegativo() {
        conta.setAtiva(false);

        ContaDeactivatedException exception =
                Assertions.assertThrows(ContaDeactivatedException.class, () -> {
                    contaService.contaAtiva(conta);
                });

        Assertions.assertEquals(exception.getMessage(), "Sua conta está desativada!");
    }

    @Test
    public void testOrigemEDestinoContaAtivaCaminhoPositivo() {
        Conta contaDestino = new Conta();
        contaDestino.setAtiva(true);

        Assertions.assertTrue(contaService.contaAtiva(conta, contaDestino));
    }

    @Test
    public void testOrigemContaAtivaCaminhoNegativo() {
        conta.setAtiva(false);
        Conta contaDestino = new Conta();
        contaDestino.setAtiva(true);

        ContaDeactivatedException exception =
                Assertions.assertThrows(ContaDeactivatedException.class, () -> {
                    contaService.contaAtiva(conta, contaDestino);
                });

        Assertions.assertEquals(exception.getMessage(), "Sua conta está desativada!");
    }

    @Test
    public void testDestinoContaAtivaCaminhoNegativo() {
        Conta contaDestino = new Conta();

        contaDestino.setAtiva(false);

        ContaDeactivatedException exception =
                Assertions.assertThrows(ContaDeactivatedException.class, () -> {
                    contaService.contaAtiva(conta, contaDestino);
                });

        Assertions.assertEquals(exception.getMessage(), "A conta destino está desativada!");
    }

    @Test
    public void testarGerarNumConta() {
        contaService.gerarNumConta(0, conta);
        Assertions.assertEquals("00001", conta.getNumConta());
    }

    @Test
    public void testDesativarConta() {
        Mockito.when(usuarioService.buscarUsuarioPorCpf("12345678910")).thenReturn(usuario);

        Mockito.when(contaRepository.save(usuario.getConta())).thenReturn(conta);

        conta.setSaldo(0);
        contaService.desativarConta(usuario.getCpf());
        Assertions.assertFalse(conta.isAtiva());
    }

    @Test
    public void testSaldoZeradoCaminhoPositivo() {
        conta.setSaldo(0);

        Assertions.assertTrue(contaService.saldoZerado(conta.getSaldo()));
    }

    @Test
    public void testSaldoZeradoCaminhoNegativo() {
        SaldoExistedException exception =
                Assertions.assertThrows(SaldoExistedException.class, () -> {
                    contaService.saldoZerado(conta.getSaldo());
                });

        Assertions.assertEquals(exception.getMessage(), "Retire seu  saldo para poder desativar sua conta !");
    }

    @Test
    public void testContaDesativadaCaminhoPositivo() {
        conta.setAtiva(false);
        Assertions.assertTrue(contaService.contaDesativada(conta));
    }

    @Test
    public void testContaDesativadaCaminhoNegativo() {
        ContaActivatedException exception =
                Assertions.assertThrows(ContaActivatedException.class, () -> {
                    contaService.contaDesativada(conta);
                });

        Assertions.assertEquals(exception.getMessage(), "Sua Conta já está ativada!");
    }

    @Test
    public void testAtivarConta() {
        Mockito.when(usuarioService.buscarUsuarioPorCpf("12345678910")).thenReturn(usuario);
        conta.setAtiva(false);
        Mockito.when(contaRepository.save(conta)).thenReturn(conta);

        contaService.ativarConta(usuario.getCpf());
        Assertions.assertTrue(conta.isAtiva());
    }

    @Test
    public void testObterSaldo() {
        Optional<Conta> contaOptional = Optional.of(conta);
        Mockito.when(contaRepository.findById("00001")).thenReturn(contaOptional);

        Assertions.assertEquals(conta.getSaldo(), contaService.obterSaldo("00001"));
    }

    @Test
    public void testSacar() {
        Mockito.when(contaRepository.findById("00001")).thenReturn(contaOptional);

        Mockito.when(usuarioService.possuiConta(usuario)).thenReturn(true);

        Mockito.when(contaRepository.save(conta)).thenReturn(conta);

        Mockito.when(operacaoRepository.save(operacao)).thenReturn(operacao);

        Assertions.assertEquals(conta.getSaldo() - 50, contaService.sacar("00001", 50));
    }

    @Test
    public void testVerificaContaCaminhoPositivo() {
        Mockito.when(contaRepository.findById("00001")).thenReturn(contaOptional);
        contaService.verificaConta("0001", "00001", 7);
    }

    @Test
    public void testVerificaContaCaminhoNegativo() {
        Mockito.when(contaRepository.findById(Mockito.anyString())).thenReturn(contaOptional);

        ContaNotFoundException exception =
                Assertions.assertThrows(ContaNotFoundException.class, () -> {
                    contaService.verificaConta("0002", "00001", 8);
                });

        Assertions.assertEquals(exception.getMessage(), "Conta de destino não encontrada!");
    }

    @Test
    public void testDepositar() {
        Mockito.when(contaRepository.findById("00001")).thenReturn(contaOptional);

        Mockito.when(contaRepository.save(conta)).thenReturn(conta);

        Mockito.when(operacaoRepository.save(operacao)).thenReturn(operacao);

        contaService.depositar(100, "0001", "00001", 7);
        Assertions.assertEquals(200, conta.getSaldo());
    }
}

