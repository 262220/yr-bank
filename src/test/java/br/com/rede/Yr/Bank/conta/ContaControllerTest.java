package br.com.rede.Yr.Bank.conta;

import br.com.rede.Yr.Bank.conta.dtos.*;
import br.com.rede.Yr.Bank.exceptions.OptionNotValidException;
import br.com.rede.Yr.Bank.security.jwt.ComponenteJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.awt.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@WebMvcTest(ContaController.class)
public class ContaControllerTest {
    @MockBean
    private ContaService contaService;
    @MockBean
    private ModelMapper modelMapper;
    @MockBean
    private ComponenteJWT componenteJWT;

    @Autowired
    private MockMvc mockMvc;

    private ObjectMapper objectMapper;
    private Conta conta;
    private Claims claims;

    @BeforeEach
    public void setUp() {
        objectMapper = new ObjectMapper().registerModule(new JavaTimeModule()).disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        conta = new Conta();
        claims = Jwts.claims();
    }

    @Test
    @WithMockUser(username = "Vinicius", password = "123456")
    public void testRotaCriarContaCaminhoPositivo() throws Exception {
        Mockito.when(contaService.criarConta(Mockito.anyString())).thenReturn(conta);

        ResultActions resultadoDaRequisicao = mockMvc
                .perform(MockMvcRequestBuilders.post("/conta")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "token 12345678910"))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    @WithMockUser(username = "Vinicius", password = "123456")
    public void testRotaObterSaldo() throws Exception {
        Mockito.when(contaService.buscarContaPorEmail(Mockito.anyString())).thenReturn(conta);

        Mockito.when(contaService.obterSaldo(Mockito.anyString())).thenReturn(conta.getSaldo());

        ResultActions resultadoDaRequisicao = mockMvc
                .perform(MockMvcRequestBuilders.get("/conta/saldo")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "token 12345678910"))
                .andExpect(MockMvcResultMatchers.status().isOk());

        String jsonResposta = resultadoDaRequisicao.andReturn().getResponse().getContentAsString();
        ObterSaldoDTO obterSaldoDTO = objectMapper.readValue(jsonResposta, ObterSaldoDTO.class);

        Assertions.assertEquals(conta.getSaldo(), obterSaldoDTO.getSaldo());
    }

    @Test
    @WithMockUser(username = "Vinicius", password = "123456")
    public void testRotaSacar() throws Exception {
        OperacaoContaDto operacaoContaDto = new OperacaoContaDto();
        operacaoContaDto.setValor(100);
        String json = objectMapper.writeValueAsString(operacaoContaDto);

        Mockito.when(contaService.buscarContaPorEmail(Mockito.anyString())).thenReturn(conta);

        Mockito.when(contaService.sacar(Mockito.anyString(), Mockito.anyDouble())).thenReturn(conta.getSaldo());
        ResultActions resultadoDaRequisicao = mockMvc
                .perform(MockMvcRequestBuilders.post("/conta/saque")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "token 12345678910")
                        .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser(username = "Vinicius", password = "123456")
    public void testRotaAtivarContaCaminhoPositivo() throws Exception {
        Mockito.when(componenteJWT.getClaims(Mockito.anyString())).thenReturn(claims);

        contaService.ativarConta(claims.getSubject());
        Mockito.verify(contaService, Mockito.times(1)).ativarConta(claims.getSubject());

        Mockito.verify(contaService, Mockito.times(0)).desativarConta(claims.getSubject());

        ResultActions resultadoDaRequisicao = mockMvc
                .perform(MockMvcRequestBuilders.put("/conta?ativar=true")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "token 12345678910"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser(username = "Vinicius", password = "123456")
    public void testRotaDesativarContaCaminhoPositivo() throws Exception {
        Mockito.when(componenteJWT.getClaims(Mockito.anyString())).thenReturn(claims);

        Mockito.verify(contaService, Mockito.times(0)).ativarConta(claims.getSubject());

        contaService.desativarConta(claims.getSubject());
        Mockito.verify(contaService, Mockito.times(1)).desativarConta(claims.getSubject());

        ResultActions resultadoDaRequisicao = mockMvc
                .perform(MockMvcRequestBuilders.put("/conta?desativar=true")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "token 12345678910"))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }

    @Test
    @WithMockUser(username = "Vinicius", password = "123456")
    public void testRotaAtivarEDesativarContaCaminhoNegativo() throws Exception {
        Mockito.when(componenteJWT.getClaims(Mockito.anyString())).thenReturn(claims);

        Mockito.verify(contaService, Mockito.times(0)).desativarConta(claims.getSubject());
        Mockito.verify(contaService, Mockito.times(0)).ativarConta(claims.getSubject());

        ResultActions resultadoDaRequisicao = mockMvc
                .perform(MockMvcRequestBuilders.put("/conta")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "token 12345678910"))
                .andExpect(MockMvcResultMatchers.status().isUnprocessableEntity());

    }

    @Test
    @WithMockUser(username = "Vinicius", password = "123456")
    public void testRotaDepositar() throws Exception {
        DepositoDto depositoDto = new DepositoDto();
        depositoDto.setAgencia("0001");
        depositoDto.setNumConta("00001");
        depositoDto.setDigito(7);
        depositoDto.setValor(100);
        String json = objectMapper.writeValueAsString(depositoDto);

        contaService.depositar(depositoDto.getValor(), depositoDto.getAgencia(), depositoDto.getNumConta(), depositoDto.getDigito());
        Mockito.verify(contaService, Mockito.times(1)).depositar(depositoDto.getValor(), depositoDto.getAgencia(), depositoDto.getNumConta(), depositoDto.getDigito());

        ResultActions resultadoDaRequisicao = mockMvc
                .perform(MockMvcRequestBuilders.post("/conta/deposito")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
                        .header("Authorization", "token 12345678910"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}



