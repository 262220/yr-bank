package br.com.rede.Yr.Bank.operacao;

import br.com.rede.Yr.Bank.conta.Conta;
import br.com.rede.Yr.Bank.conta.ContaRepository;
import br.com.rede.Yr.Bank.conta.ContaService;
import br.com.rede.Yr.Bank.enuns.TipoTransacao;
import br.com.rede.Yr.Bank.operacao.Dtos.DataOperacaoDto;
import br.com.rede.Yr.Bank.operacao.Dtos.PagamentoDto;
import br.com.rede.Yr.Bank.operacao.Dtos.TransferenciaDto;
import br.com.rede.Yr.Bank.security.jwt.ComponenteJWT;
import br.com.rede.Yr.Bank.usuario.Usuario;
import br.com.rede.Yr.Bank.usuario.UsuarioController;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@WebMvcTest(OperacaoController.class)
public class OperacaoControllerTest {
    @MockBean
    private ContaService contaService;
    @MockBean
    private ContaRepository contaRepository;
    @MockBean
    private ModelMapper modelMapper;
    @MockBean
    private ComponenteJWT componenteJWT;
    @MockBean
    private OperacaoService operacaoService;
    @Autowired
    private MockMvc mockMvc;

    private Conta conta;
    private ObjectMapper objectMapper;
    private Operacao operacao;
    private DataOperacaoDto dataOperacaoDto;
    private PagamentoDto pagamentoDto;
    private TransferenciaDto transferenciaDto;
    private Optional<Conta> contaOptional;
private Usuario usuario;
    @BeforeEach
    public void setUp() {
        objectMapper = new ObjectMapper();
        conta = new Conta();
        conta.setAgencia("0001");
        conta.setNumConta("00001");
        conta.setSaldo(0);
        conta.setAtiva(true);

        usuario = new Usuario();
usuario.setEmail("vini@gmail.com");
usuario.setCpf("12345678910");
usuario.setConta(conta);
conta.setTitular(usuario);

dataOperacaoDto = new DataOperacaoDto();
        dataOperacaoDto.setDataOperacao("20/09/2021");

        pagamentoDto = new PagamentoDto();
        pagamentoDto.setCodBarras("000000000000000000000000000000000000000000000000");

        operacao = new Operacao();
        operacao.setOrigem(conta);
        operacao.setTipoTransacao(TipoTransacao.NEGATIVA);
        operacao.setHoraOperacao(LocalTime.now());
        operacao.setDataOperacao(LocalDate.now());
        operacao.setDestino("00002");

        contaOptional = Optional.of(conta);
        transferenciaDto = new TransferenciaDto();
        transferenciaDto.setValor(100);
        transferenciaDto.setAgencia("0001");
        transferenciaDto.setNumConta("00002");
        transferenciaDto.setDigito(7);
    }

    @Test
    @WithMockUser(username = "teste", password = "123456")
    public void testarMetodoPagarBoleto() throws Exception {
        Optional<Conta> contaOptional = Optional.of(conta);
        Mockito.when(contaService.buscarContaPorNumero(Mockito.anyString())).thenReturn(conta);
        Mockito.when(contaRepository.findById(Mockito.anyString())).thenReturn(contaOptional);
        Mockito.when(operacaoService.pagarBoleto(Mockito.anyString(), Mockito.anyString())).thenReturn(operacao);
        String json = objectMapper.writeValueAsString(pagamentoDto);
        ResultActions resultadoRequisicao = mockMvc
                .perform(MockMvcRequestBuilders.get("/operacoes").contentType(MediaType.APPLICATION_JSON)
                        .content(json).header("Authorization", "Token ")).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser(username = "teste", password = "123456")
    public void testarMetodoExibirOperacoes() throws Exception {
        List<Operacao> operacoes = new ArrayList<>();
        Optional<Conta> contaOptional = Optional.of(conta);
        Mockito.when(contaService.buscarContaPorNumero(Mockito.anyString())).thenReturn(conta);
        Mockito.when(contaRepository.findById(Mockito.anyString())).thenReturn(contaOptional);
        Mockito.when(operacaoService.filtrarOperacoesPorData(Mockito.any(Conta.class), Mockito.anyString())).thenReturn(operacoes);
        String json = objectMapper.writeValueAsString(dataOperacaoDto);
        ResultActions resultadoRequisicao = mockMvc
                .perform(MockMvcRequestBuilders.get("/operacoes").contentType(MediaType.APPLICATION_JSON)
                        .content(json).header("Authorization", "Token ")).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser(username = "Vinicius", password = "123456")
    public void testRotaTransferir() throws Exception {
        String json = objectMapper.writeValueAsString(transferenciaDto);

        Mockito.when(contaService.buscarContaPorNumero(Mockito.anyString())).thenReturn(conta);

        Mockito.when(operacaoService.transferir(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyInt(), Mockito.anyDouble())).thenReturn(operacao);

        ResultActions resultadoDaRequisicao = mockMvc
                .perform(MockMvcRequestBuilders.post("/operacoes/transferencia")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
                        .header("Authorization", "token 12345678910"))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }

}
