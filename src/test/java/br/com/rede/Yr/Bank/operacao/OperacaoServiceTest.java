package br.com.rede.Yr.Bank.operacao;

import br.com.rede.Yr.Bank.conta.Conta;
import br.com.rede.Yr.Bank.conta.ContaService;
import br.com.rede.Yr.Bank.enuns.TipoTransacao;
import br.com.rede.Yr.Bank.exceptions.BoletoNotValidException;
import br.com.rede.Yr.Bank.exceptions.ContaDuplicatedException;
import br.com.rede.Yr.Bank.exceptions.ContaNotValidException;
import br.com.rede.Yr.Bank.usuario.Usuario;
import br.com.rede.Yr.Bank.usuario.UsuarioService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class OperacaoServiceTest {
    @Autowired
    private OperacaoService operacaoService;

    @MockBean
    private OperacaoRepository operacaoRepository;

    @MockBean
    private UsuarioService usuarioService;

    @MockBean
    private ContaService contaService;

    private Usuario usuario;
    private Conta origem;
    private Conta destino;
    private Operacao operacao;

    @BeforeEach
    public void Setup(){
        usuario = new Usuario();
        operacao = new Operacao();
        origem = new Conta();
        destino = new Conta();
    }

    @Test
    public void testarMetodoVerificaOperacaoCaminhoPositivo(){
        origem.setNumConta("00001");
        destino.setNumConta("00002");
        operacaoService.verificaOperacao(origem,destino);
    }

    @Test
    public void testarMetodoVerificaOperacaoCaminhoNegativo(){
        origem.setNumConta("00001");
        destino.setNumConta("00001");

        ContaNotValidException exception = Assertions
                .assertThrows(ContaNotValidException.class, () -> {operacaoService.verificaOperacao(origem,destino);});
        Assertions.assertEquals("Conta de destino inválida!", exception.getMessage());
    }

    @Test
    public void testarMetodoSalvarRegistroOperacao(){
        Mockito.when(operacaoRepository.save(Mockito.any(Operacao.class))).thenReturn(operacao);

        Assertions.assertEquals(operacao,operacaoService.salvarRegistroOperacao(origem,destino.getNumConta(),50, TipoTransacao.NEGATIVA));
    }

    @Test
    public void testarMetodoTransferir(){
        origem.setNumConta("00001");
        destino.setNumConta("00002");
        Mockito.when(contaService.buscarContaPorEmail(Mockito.anyString())).thenReturn(origem);
        Mockito.when(contaService.buscarContaPorNumero(Mockito.anyString())).thenReturn(destino);
        Mockito.when(operacaoRepository.save(Mockito.any(Operacao.class))).thenReturn(operacao);
        Assertions.assertEquals(operacao,operacaoService.transferir("","","",7,50));
    }

    @Test
    public void testarMetodoPagarBoleto(){
        String codBarras = "00000000000000000000000000000000000000000005000";
        Mockito.when(contaService.buscarContaPorNumero(Mockito.anyString())).thenReturn(origem);
        Mockito.when(operacaoRepository.save(Mockito.any(Operacao.class))).thenReturn(operacao);
        Assertions.assertEquals(operacao,operacaoService.pagarBoleto("",codBarras));
    }

    @Test
    public void testarMetodoVerificarValor(){
        String codBarras = "00000000000000000000000000000000000000000007000";
        Assertions.assertEquals(70.00,operacaoService.verificaValor(codBarras));
    }

    @Test
    public void testarMetodoVerificarBoletoCaminhoNegativoRotaInvalido(){
        String codBarras = "";

        BoletoNotValidException exception = Assertions
                .assertThrows(BoletoNotValidException.class, () -> {operacaoService.verificarBoleto(codBarras);});
        Assertions.assertEquals("Boleto inválido!", exception.getMessage());
    }

    @Test
    public void testarMetodoVerificarBoletoCaminhoNegativoRotaVencido(){
        String codBarras = "03390500954014481606906809350314335760000007500";

        BoletoNotValidException exception = Assertions
                .assertThrows(BoletoNotValidException.class, () -> {operacaoService.verificarBoleto(codBarras);});
        Assertions.assertEquals("Boleto vencido!", exception.getMessage());
    }

    @Test
    public void testarMetodoVerificarDataValidadeBoleto(){
        String codBarras = "23793381286006890119286000063304487580000011000";
        Assertions.assertEquals(LocalDate.parse("2021-09-29"), operacaoService.verificarDataValidadeBoleto(codBarras));
    }

    @Test
    public void testarMetodoFiltrarOperacoesPorData(){
        String data = "29/09/2021";
        List<Operacao> negativas = new ArrayList<>();
        List<Operacao> positivas = new ArrayList<>();
        Mockito.when(operacaoRepository.buscarOperacoesNegativas(Mockito.any(Conta.class),Mockito.any(LocalDate.class))).thenReturn(negativas);
        Mockito.when(operacaoRepository.buscarOperacoesPositivas(Mockito.anyString(),Mockito.any(LocalDate.class))).thenReturn(positivas);
        Assertions.assertEquals(negativas,operacaoService.filtrarOperacoesPorData(origem,data));
    }

}
