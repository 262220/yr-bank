package br.com.rede.Yr.Bank.usuario;

import br.com.rede.Yr.Bank.endereco.dtos.EnderecoDto;
import br.com.rede.Yr.Bank.security.jwt.ComponenteJWT;
import br.com.rede.Yr.Bank.usuario.dtos.CadastrarUsuarioDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(UsuarioController.class)
public class UsuarioControllerTest {
    @MockBean
    private UsuarioRepository usuarioRepository;
    @MockBean
    private UsuarioService usuarioService;
    @MockBean
    private ComponenteJWT componenteJWT;
    @MockBean
    private ModelMapper modelMapper;
    @Autowired
    private MockMvc mockMvc;

    private ObjectMapper objectMapper;
    private CadastrarUsuarioDTO usuarioDTO;
    private Usuario usuario;
    private EnderecoDto endereco;
    private Claims claims;

    @BeforeEach
    public void Setup(){
        claims = Jwts.claims();
        endereco = new EnderecoDto();
        usuarioDTO = new CadastrarUsuarioDTO();
        usuarioDTO.setCpf("22222222222");
        usuarioDTO.setNome("Vinícius");
        usuarioDTO.setEmail("vinicius@gmail.com");
        usuarioDTO.setSenha("123456");
        usuarioDTO.setTelefone("1145454545");
        endereco.setCep("00000000");
        endereco.setLogradouro("Rua teste");
        endereco.setNumero(222);
        endereco.setBairro("JD. Teste");
        endereco.setCidade("Teste");
        endereco.setEstado("Teste");
        usuarioDTO.setEndereco(endereco);
        objectMapper = new ObjectMapper();
    }

    @Test
    public void testarMetodoCadasatrarUsuario() throws Exception{
        usuario = modelMapper.map(usuarioDTO,Usuario.class);
        Mockito.when(usuarioRepository.save(Mockito.any(Usuario.class))).thenReturn(usuario);
        usuarioService.cadastrarUsuario(usuario);
        String json = objectMapper.writeValueAsString(usuarioDTO);
        ResultActions resultadoRequisicao = mockMvc
                .perform(MockMvcRequestBuilders.post("/usuario").contentType(MediaType.APPLICATION_JSON)
                        .content(json)).andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    @WithMockUser(username = "vinicius", password = "123456")
    public void testarMetodoExibirPerfil() throws Exception {
        usuario = modelMapper.map(usuarioDTO,Usuario.class);
        Mockito.when(usuarioService.buscarUsuarioPorCpf(Mockito.anyString())).thenReturn(usuario);
        Mockito.when(componenteJWT.getClaims(Mockito.anyString())).thenReturn(claims);
        ResultActions resultadoRequisicao = mockMvc
                .perform(MockMvcRequestBuilders.get("/usuario").contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Token ")).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser(username = "vinicius", password = "123456")
    public void testarMetodoAtualizarUsuario() throws Exception {
        usuario = modelMapper.map(usuarioDTO,Usuario.class);
        Mockito.when(usuarioRepository.save(Mockito.any(Usuario.class))).thenReturn(usuario);
        usuarioService.atualizarUsuario(usuario,"");
        String json = objectMapper.writeValueAsString(usuarioDTO);
        ResultActions resultadoRequisicao = mockMvc
                .perform(MockMvcRequestBuilders.put("/usuario").contentType(MediaType.APPLICATION_JSON)
                        .content(json).header("Authorization","Token ")).andExpect(MockMvcResultMatchers.status().isOk());
    }

}
