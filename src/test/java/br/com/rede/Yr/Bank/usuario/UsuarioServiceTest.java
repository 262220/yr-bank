package br.com.rede.Yr.Bank.usuario;

import br.com.rede.Yr.Bank.conta.Conta;
import br.com.rede.Yr.Bank.endereco.Endereco;
import br.com.rede.Yr.Bank.endereco.EnderecoService;
import br.com.rede.Yr.Bank.exceptions.ContaDuplicatedException;
import br.com.rede.Yr.Bank.exceptions.ContaNotFoundException;
import br.com.rede.Yr.Bank.exceptions.UsuarioDuplicatedException;
import br.com.rede.Yr.Bank.exceptions.UsuarioNotFoundException;
import br.com.rede.Yr.Bank.usuario.Usuario;
import br.com.rede.Yr.Bank.usuario.UsuarioRepository;
import br.com.rede.Yr.Bank.usuario.UsuarioService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpInputMessage;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Optional;

@SpringBootTest
public class UsuarioServiceTest {
    @Autowired
    private UsuarioService usuarioService;

    @MockBean
    private UsuarioRepository usuarioRepository;

    @MockBean
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @MockBean
    private EnderecoService enderecoService;

    private Usuario usuario;
    private Conta conta;

    @BeforeEach
    public void Setup(){
        usuario = new Usuario();
        conta = new Conta();
    }

    @Test
    public void testarMetodoBuscarUsuarioPorEmailCaminhoPositivo(){
        Optional<Usuario> usuarioOptional = Optional.of(usuario);
        Mockito.when(usuarioRepository.findByEmailContains(Mockito.anyString())).thenReturn(usuarioOptional);

        Assertions.assertEquals(usuario, usuarioService.buscarUsuarioPorEmail("a"));
    }

    @Test
    public void testarMetodoBuscarUsuarioPorEmailCaminhoNegativo(){
        Optional<Usuario> usuarioOptional = Optional.empty();
        Mockito.when(usuarioRepository.findByEmailContains(Mockito.anyString())).thenReturn(usuarioOptional);

        UsuarioNotFoundException exception = Assertions
                .assertThrows(UsuarioNotFoundException.class, () -> {usuarioService.buscarUsuarioPorEmail("");});
        Assertions.assertEquals("Usuário não encontrado!", exception.getMessage());
    }

    @Test
    public void testarMetodoBuscarUsuarioPorCpfCaminhoPositivo(){
        Optional<Usuario> usuarioOptional = Optional.of(usuario);
        Mockito.when(usuarioRepository.findById(Mockito.anyString())).thenReturn(usuarioOptional);

        Assertions.assertEquals(usuario, usuarioService.buscarUsuarioPorCpf("222"));
    }

    @Test
    public void testarMetodoBuscarUsuarioPorCpfCaminhoNegativo(){
        Optional<Usuario> usuarioOptional = Optional.empty();
        Mockito.when(usuarioRepository.findByEmailContains(Mockito.anyString())).thenReturn(usuarioOptional);

        UsuarioNotFoundException exception = Assertions
                .assertThrows(UsuarioNotFoundException.class, () -> {usuarioService.buscarUsuarioPorEmail("");});
        Assertions.assertEquals("Usuário não encontrado!", exception.getMessage());
    }

    @Test
    public void testarMetodoPossuiContaCaminhoPositivo() {
        usuario.setConta(conta);
        Assertions.assertTrue(usuarioService.possuiConta(usuario));
    }

    @Test
    public void testarMetodoPossuiContaCaminhoNegativo(){
        ContaNotFoundException exception = Assertions
                .assertThrows(ContaNotFoundException.class, () -> {usuarioService.possuiConta(usuario);});
        Assertions.assertEquals("Você ainda não possui uma conta!", exception.getMessage());
    }

    @Test
    public void testarMetodoPossuiContaCpfCaminhoPositivo() {
        usuario.setCpf("222");
        Optional<Usuario> usuarioOptional = Optional.of(usuario);
        Mockito.when(usuarioRepository.findById(Mockito.anyString())).thenReturn(usuarioOptional);
        Assertions.assertTrue(usuarioService.possuiConta(usuarioOptional.get().getCpf()));
    }

    @Test
    public void testarMetodoPossuiContaCpfCaminhoNegativo(){
        usuario.setCpf("222");
        usuario.setConta(conta);

        Optional<Usuario> usuarioOptional = Optional.of(usuario);
        Mockito.when(usuarioRepository.findById(Mockito.anyString())).thenReturn(usuarioOptional);
        ContaDuplicatedException exception = Assertions
                .assertThrows(ContaDuplicatedException.class, () -> {usuarioService.possuiConta("222");});
        Assertions.assertEquals("Você já possui uma conta!", exception.getMessage());
    }

    @Test
    public void testarMetodoVerificarUsuarioExistenteCaminhoPositivo(){
        usuarioService.verficarUsuarioExistente(usuario);
        Mockito.verify(usuarioRepository,Mockito.times(1)).existsById(usuario.getCpf());
    }

    @Test
    public void testarMetodoVerificarUsuarioExistenteCaminhoNegativo(){
        usuarioService.verficarUsuarioExistente(usuario);
        Mockito.verify(usuarioRepository,Mockito.times(0)).existsById("222");

    }

    @Test
    public void testarMetodoCadastrarUsuario(){
        Endereco endereco = new Endereco();
        String encode = "";
        Mockito.when(bCryptPasswordEncoder.encode(usuario.getSenha())).thenReturn(encode);
        usuario.setSenha(encode);
        usuario.setEndereco(endereco);
        usuario.getEndereco().setMorador(usuario);
        usuarioService.cadastrarUsuario(usuario);
        Mockito.when(usuarioRepository.save(Mockito.any(Usuario.class))).thenReturn(usuario);
        Mockito.verify(usuarioRepository,Mockito.times(1)).save(usuario);
    }

    @Test
    public void testarMetodoAtualizarUsuario(){
        Optional<Usuario> usuarioOptional = Optional.of(usuario);
        Usuario userAtual = new Usuario();
        usuario.setNome("Xablau");
        usuario.setEmail("xablau@xablau");
        userAtual.setNome("Xablau2");
        Endereco endereco = new Endereco();
        usuario.setEndereco(endereco);
        Mockito.when(usuarioRepository.findByEmailContains(Mockito.anyString())).thenReturn(usuarioOptional);
        usuarioService.atualizarUsuario(userAtual,usuario.getEmail());
        Assertions.assertEquals(userAtual.getNome(),usuario.getNome());
    }

}
