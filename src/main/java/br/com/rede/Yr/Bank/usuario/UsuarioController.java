package br.com.rede.Yr.Bank.usuario;

import br.com.rede.Yr.Bank.security.jwt.ComponenteJWT;
import br.com.rede.Yr.Bank.usuario.dtos.AtualizarUsuarioDTO;
import br.com.rede.Yr.Bank.usuario.dtos.CadastrarUsuarioDTO;
import br.com.rede.Yr.Bank.usuario.dtos.UsuarioDto;
import io.jsonwebtoken.Claims;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {
    private UsuarioService usuarioService;
    private ModelMapper modelMapper;
    private ComponenteJWT componenteJWT;

    @Autowired
    public UsuarioController(UsuarioService usuarioService, ModelMapper modelMapper, ComponenteJWT componenteJWT) {
        this.usuarioService = usuarioService;
        this.modelMapper = modelMapper;
        this.componenteJWT = componenteJWT;
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public void cadastrarUsuarioDTO(@RequestBody @Valid CadastrarUsuarioDTO cadastrarUsuarioDTO) {
        usuarioService.cadastrarUsuario(modelMapper.map(cadastrarUsuarioDTO, Usuario.class));
    }

    @GetMapping
    public UsuarioDto exibirPerfil(HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        Claims claims = componenteJWT.getClaims(token.substring(6));
        Usuario usuario = usuarioService.buscarUsuarioPorCpf(claims.getSubject());

        return modelMapper.map(usuario, UsuarioDto.class);
    }

    @PutMapping
    public void atualizarUsuario(@RequestBody @Valid AtualizarUsuarioDTO atualizarUsuarioDTO, Authentication authentication){
        Usuario usuario = modelMapper.map(atualizarUsuarioDTO,Usuario.class);
        usuarioService.atualizarUsuario(usuario,authentication.getName());
    }

}
