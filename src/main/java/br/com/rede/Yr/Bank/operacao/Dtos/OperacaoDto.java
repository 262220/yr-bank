package br.com.rede.Yr.Bank.operacao.Dtos;

import br.com.rede.Yr.Bank.conta.dtos.ContaDto;

import java.time.LocalDate;
import java.time.LocalTime;

public class OperacaoDto {
    private ContaDto origem;
    private String destino;
    private LocalDate data = LocalDate.now();
    private LocalTime hora = LocalTime.now();
    private double valor;

    public OperacaoDto() {
    }

    public ContaDto getOrigem() {
        return origem;
    }

    public void setOrigem(ContaDto origim) {
        this.origem = origim;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public LocalTime getHora() {
        return hora;
    }

    public void setHora(LocalTime hora) {
        this.hora = hora;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
