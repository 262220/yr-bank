package br.com.rede.Yr.Bank.endereco;

import br.com.rede.Yr.Bank.exceptions.EnderecoNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EnderecoService {
    @Autowired
    private EnderecoRepository enderecoRepository;

    public Endereco buscarEnderecoPorId(int id){
        Optional<Endereco> enderecoOptional = enderecoRepository.findById(id);

        enderecoOptional.orElseThrow(() -> new EnderecoNotFoundException("Nenhum endereço encontrado!"));

        return enderecoOptional.get();
    }

    public Endereco atualizarEndereco(int id, Endereco endereco){
        Endereco enderecoAtual = buscarEnderecoPorId(id);
        enderecoAtual.setLogradouro(endereco.getLogradouro());
        enderecoAtual.setNumero(endereco.getNumero());
        enderecoAtual.setComplemento(endereco.getComplemento());
        enderecoAtual.setBairro(endereco.getBairro());
        enderecoAtual.setCidade(endereco.getCidade());
        enderecoAtual.setEstado(endereco.getEstado());
        enderecoAtual.setCep(endereco.getCep());

        return enderecoRepository.save(enderecoAtual);
    }

}

