package br.com.rede.Yr.Bank.conta.dtos;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class RespostaDepositoDto {
    private double valorDepositado;
    private LocalDateTime dataDeposito;

    public RespostaDepositoDto() {
    }

    public RespostaDepositoDto(double valorDepositado, LocalDateTime dataDeposito) {
        this.valorDepositado = valorDepositado;
        this.dataDeposito = dataDeposito;
    }

    public String getDataDeposito() {
        return formatar(dataDeposito);
    }

    public void setDataDeposito(LocalDateTime dataDeposito) {
        this.dataDeposito = dataDeposito;
    }

    public double getValorDepositado() {
        return valorDepositado;
    }

    public void setValorDepositado(double valorDepositado) {
        this.valorDepositado = valorDepositado;
    }

    public String formatar(LocalDateTime localDate){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        return localDate.format(formatter);
    }
}
