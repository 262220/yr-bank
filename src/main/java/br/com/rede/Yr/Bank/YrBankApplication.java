package br.com.rede.Yr.Bank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YrBankApplication {

    public static void main(String[] args) {
        SpringApplication.run(YrBankApplication.class, args);
    }

}
