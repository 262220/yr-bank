package br.com.rede.Yr.Bank.exceptions;

public class ContaActivatedException extends RuntimeException {
    public ContaActivatedException(String message) {
        super(message);
    }
}
