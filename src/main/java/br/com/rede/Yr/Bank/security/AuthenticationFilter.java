package br.com.rede.Yr.Bank.security;

import br.com.rede.Yr.Bank.security.dtos.LoginDTO;
import br.com.rede.Yr.Bank.security.jwt.ComponenteJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private ComponenteJWT componenteJWT;
    private AuthenticationManager authenticationManager;

    public AuthenticationFilter(ComponenteJWT componenteJWT, AuthenticationManager authenticationManager) {
        this.componenteJWT = componenteJWT;
        this.authenticationManager = authenticationManager;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            LoginDTO login = objectMapper.readValue(request.getInputStream(), LoginDTO.class);

            UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                    login.getEmail(), login.getSenha(), new ArrayList<>()
            );

            Authentication auth = authenticationManager.authenticate(authToken);
            return auth;
        }catch (IOException e){
            throw new RuntimeException("Acesso negado!");
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        UsuarioLogin usuarioLogin = (UsuarioLogin) authResult.getPrincipal();
        String email = usuarioLogin.getEmail();
        String cpf = usuarioLogin.getCpf();

        String token = componenteJWT.gerarToken(cpf,email);

        response.setHeader("Access-Control-Expose-Headers", "Authorization");
        response.addHeader("Authorization", "Token "+token);

    }
}
