package br.com.rede.Yr.Bank.endereco;

import br.com.rede.Yr.Bank.usuario.Usuario;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "enderecos")
public class Endereco {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 8, nullable = false)
    private String cep;
    @Column(length = 100,nullable = false)
    private String logradouro;
    @Column(length = 5,nullable = false)
    private int numero;
    @Column(length = 50, nullable = true)
    private String complemento;
    @Column(length = 30,nullable = false)
    private String Bairro;
    @Column(length = 50, nullable = false)
    private String cidade;
    @Column(length = 10, nullable = false)
    private String estado;

    @OneToOne(cascade=CascadeType.ALL)
    private Usuario morador;

    public Endereco() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return Bairro;
    }

    public void setBairro(String bairro) {
        Bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Usuario getMorador() {
        return morador;
    }

    public void setMorador(Usuario morador) {
        this.morador = morador;
    }
}
