package br.com.rede.Yr.Bank.conta.dtos;

import javax.validation.constraints.Min;

public class OperacaoContaDto {
    @Min(value = 1, message = "{validacao.conta}")
    private double valor;

    public OperacaoContaDto() {
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
