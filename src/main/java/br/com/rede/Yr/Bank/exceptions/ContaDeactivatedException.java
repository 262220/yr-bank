package br.com.rede.Yr.Bank.exceptions;

public class ContaDeactivatedException extends RuntimeException{
    public ContaDeactivatedException(String message) {
        super(message);
    }
}
