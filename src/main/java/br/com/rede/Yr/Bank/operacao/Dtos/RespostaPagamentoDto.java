package br.com.rede.Yr.Bank.operacao.Dtos;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class RespostaPagamentoDto {
    private double valorPago;
    private LocalDate vencimento;
    private LocalDate dataPagamento;

    public RespostaPagamentoDto() {
    }

    public RespostaPagamentoDto(double valorPago, LocalDate dataPagamento, LocalDate vencimento) {
        this.valorPago = valorPago;
        this.dataPagamento = dataPagamento;
        this.vencimento = vencimento;
    }

    public double getValorPago() {
        return valorPago;
    }

    public void setValorPago(double valorPago) {
        this.valorPago = valorPago;
    }

    public String getVencimento() {
        return formatar(vencimento);
    }

    public void setVencimento(LocalDate vencimento) {
        this.vencimento = vencimento;
    }

    public String getDataPagamento() {
        return formatar(dataPagamento);
    }

    public void setDataPagamento(LocalDate dataPagamento) {
        this.dataPagamento = dataPagamento;
    }

    public String formatar(LocalDate localDate){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return localDate.format(formatter);
    }

}
