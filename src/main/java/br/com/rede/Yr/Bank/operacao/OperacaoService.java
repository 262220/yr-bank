package br.com.rede.Yr.Bank.operacao;


import br.com.rede.Yr.Bank.conta.Conta;
import br.com.rede.Yr.Bank.conta.ContaService;
import br.com.rede.Yr.Bank.enuns.TipoTransacao;
import br.com.rede.Yr.Bank.exceptions.BoletoNotValidException;
import br.com.rede.Yr.Bank.exceptions.ContaNotFoundException;
import br.com.rede.Yr.Bank.exceptions.ContaNotValidException;
import br.com.rede.Yr.Bank.exceptions.DataNotValidException;
import br.com.rede.Yr.Bank.usuario.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class OperacaoService {
    private OperacaoRepository operacaoRepository;
    private ContaService contaService;
    private UsuarioService usuarioService;

    @Autowired
    public OperacaoService(OperacaoRepository operacaoRepository, ContaService contaService, UsuarioService usuarioService) {
        this.operacaoRepository = operacaoRepository;
        this.contaService = contaService;
        this.usuarioService = usuarioService;
    }

    public Operacao transferir(String email, String agencia, String numConta, int digito, double valor) {
        Conta origem = contaService.buscarContaPorEmail(email);
        usuarioService.possuiConta(origem.getTitular());

        Conta destino = contaService.buscarContaPorNumero(numConta);
        contaService.verificaConta(destino.getAgencia(), destino.getNumConta(), destino.getDigito());
        verificaOperacao(origem, destino);

        if (contaService.contaAtiva(origem, destino) && contaService.saldoSuficiente(valor, origem.getSaldo())) {
            double saldoOrigem = origem.getSaldo() - valor;
            double saldoDestino = destino.getSaldo() + valor;
            origem.setSaldo(saldoOrigem);
            destino.setSaldo(saldoDestino);
        }

        contaService.atualizarConta(origem);
        contaService.atualizarConta(destino);

        salvarRegistroOperacao(origem, destino.getNumConta(), valor, TipoTransacao.POSITIVA);
        return salvarRegistroOperacao(origem, destino.getNumConta(), valor, TipoTransacao.NEGATIVA);
    }

    public Operacao salvarRegistroOperacao(Conta origem, String destino, double valor, TipoTransacao tipoOperacao) {
        Operacao operacao = new Operacao();
        operacao.setTipoTransacao(tipoOperacao);
        operacao.setOrigem(origem);
        operacao.setDestino(destino);
        operacao.setValor(valor);
        operacao.setDataOperacao(LocalDate.now());
        operacao.setHoraOperacao(LocalTime.now());

        return operacaoRepository.save(operacao);
    }

    public void verificaOperacao(Conta origem, Conta destino) {
        if (origem.getNumConta().equals(destino.getNumConta())) {
            throw new ContaNotValidException("Conta de destino inválida!");
        }
    }

    public Operacao pagarBoleto(String numConta, String codBarras) {
        verificarBoleto(codBarras);

        double valor = verificaValor(codBarras);

        String destino = codBarras.substring(0, 3);

        Conta conta = contaService.buscarContaPorNumero(numConta);
        usuarioService.possuiConta(conta.getTitular());

        double novoSaldo = conta.getSaldo() - valor;

        if (contaService.saldoSuficiente(valor, conta.getSaldo())) {
            conta.setSaldo(novoSaldo);
        }

        return salvarRegistroOperacao(contaService.atualizarConta(conta), destino, valor, TipoTransacao.NEGATIVA);
    }

    public double verificaValor(String codBarras) {

        String valor = codBarras.substring(41);

        if(valor.equals("000000")){
            throw new BoletoNotValidException("Valor inválido!");
        }

        valor = valor.substring(0, 4) + "." + valor.substring(4, valor.length());

        return Double.parseDouble(valor);
    }

    public void verificarBoleto(String codBarras) {
        if (codBarras.length() != 47) {
            throw new BoletoNotValidException("Boleto inválido!");
        } else if (verificarDataValidadeBoleto(codBarras).isBefore(LocalDate.now())) {
            throw new BoletoNotValidException("Boleto vencido!");
        }
    }

    public LocalDate verificarDataValidadeBoleto(String codBarras) {
        int dias = Integer.parseInt(codBarras.substring(33, 37));

        LocalDate dataBase = LocalDate.parse("1997-10-07");
        LocalDate dataValidade;

        if (dias != 0) {
            dataValidade = dataBase.plusDays(dias);
        } else {
            dataValidade = LocalDate.now();
        }
        return dataValidade;
    }

    public LocalDate formatarData(String data){
        if(data.length() != 10){
            throw new DataNotValidException("Data inválida!");
        }

        String dia = data.substring(0,2);
        String mes = data.substring(3,5);
        String ano = data.substring(6,10);

        try{
            return LocalDate.parse(ano+"-"+mes+"-"+dia);
        }catch (Exception e){
            throw new DataNotValidException("Data inválida!");
        }
    }

    public List<Operacao> filtrarOperacoesPorData(Conta conta, String dataOperacao) {
        usuarioService.possuiConta(conta.getTitular());
        LocalDate data = formatarData(dataOperacao);
        List<Operacao> operacoesPositivas = operacaoRepository.buscarOperacoesPositivas(conta.getNumConta(), data);
        List<Operacao> operacoesNegativas = operacaoRepository.buscarOperacoesNegativas(conta, data);
        operacoesPositivas.iterator().forEachRemaining(operacoesNegativas::add);

        return operacoesNegativas;
    }
}

