package br.com.rede.Yr.Bank.operacao;

import br.com.rede.Yr.Bank.conta.Conta;
import br.com.rede.Yr.Bank.enuns.TipoTransacao;
import org.apache.tomcat.jni.Local;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.util.List;

public interface OperacaoRepository extends CrudRepository<Operacao, Integer> {

    @Query("select o from Operacao o where o.origem=?1 and o.dataOperacao=?2 and o.tipoTransacao='NEGATIVA'")
    List<Operacao> buscarOperacoesNegativas(Conta conta, LocalDate dataOperacao);

    @Query("select o from Operacao o where o.destino=?1 and o.dataOperacao=?2 and o.tipoTransacao='POSITIVA'")
    List<Operacao> buscarOperacoesPositivas(String numConta, LocalDate dataOperacao);

}

