package br.com.rede.Yr.Bank.usuario.dtos;

import br.com.rede.Yr.Bank.conta.dtos.ContaDto;
import br.com.rede.Yr.Bank.endereco.dtos.EnderecoDto;

public class UsuarioDto {
    private String nome;
    private String cpf;
    private String email;
    private String telefone;
    private EnderecoDto endereco;
    private ContaDto conta;

    public UsuarioDto() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public EnderecoDto getEndereco() {
        return endereco;
    }

    public void setEndereco(EnderecoDto endereco) {
        this.endereco = endereco;
    }

    public ContaDto getConta() {
        return conta;
    }

    public void setConta(ContaDto conta) {
        this.conta = conta;
    }
}

