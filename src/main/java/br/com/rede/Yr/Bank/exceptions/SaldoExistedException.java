package br.com.rede.Yr.Bank.exceptions;

public class SaldoExistedException extends RuntimeException {
    public SaldoExistedException(String message) {
        super(message);
    }
}
