package br.com.rede.Yr.Bank.operacao;

import br.com.rede.Yr.Bank.conta.Conta;
import br.com.rede.Yr.Bank.conta.ContaService;

import br.com.rede.Yr.Bank.operacao.Dtos.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/operacoes")
public class OperacaoController {
    private OperacaoService operacaoService;
    private ModelMapper modelMapper;
    private ContaService contaService;

    @Autowired
    public OperacaoController(OperacaoService operacaoService, ModelMapper modelMapper, ContaService contaService) {
        this.operacaoService = operacaoService;
        this.modelMapper = modelMapper;
        this.contaService = contaService;
    }

    @PostMapping("/pagamento")
    public RespostaPagamentoDto pagarBoleto(@RequestBody @Valid PagamentoDto pagamentoDto, Authentication authentication) {
        String numConta = contaService.buscarContaPorEmail(authentication.getName()).getNumConta();
        Operacao operacao = operacaoService.pagarBoleto(numConta, pagamentoDto.getCodBarras());

        return new RespostaPagamentoDto(operacao.getValor(), operacao.getDataOperacao(), operacaoService.verificarDataValidadeBoleto(pagamentoDto.getCodBarras()));
    }

    @PostMapping("/transferencia")
    public RespostaTransferenciaDTO efetuarTransferencia(@RequestBody @Valid TransferenciaDto transferenciaDto, Authentication authentication) {
        String email = authentication.getName();
        Conta destino = contaService.buscarContaPorNumero(transferenciaDto.getNumConta());
        Operacao operacao = operacaoService.transferir(email, transferenciaDto.getAgencia(), transferenciaDto.getNumConta(), transferenciaDto.getDigito(), transferenciaDto.getValor());

        return new RespostaTransferenciaDTO(destino.getTitular().getNome(), destino.getTitular().getCpf(), destino.getAgencia(), destino.getNumConta(), destino.getDigito(), operacao.getValor(), LocalDateTime.now());
    }

    @GetMapping
    public List<ExibirOperacoesDto> exibirOperacoes(@RequestBody DataOperacaoDto dataOperacaoDto, Authentication authentication) {
        Conta conta = contaService.buscarContaPorEmail(authentication.getName());

        return operacaoService.filtrarOperacoesPorData(conta, dataOperacaoDto.getDataOperacao()).stream().map(operacao -> modelMapper.map(operacao, ExibirOperacoesDto.class)).collect(Collectors.toList());
    }
}

