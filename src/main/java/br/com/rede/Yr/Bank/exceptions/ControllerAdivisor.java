package br.com.rede.Yr.Bank.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ControllerAdivisor {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public MensagemDeErro manipularExceptionsDeValidacao(MethodArgumentNotValidException exception) {
        List<FieldError> fieldErrors = exception.getBindingResult().getFieldErrors();
        List<Erro> erros = fieldErrors.stream().map(objeto -> new Erro(objeto.getField(), objeto.getDefaultMessage()))
                .collect(Collectors.toList());

        return new MensagemDeErro(400, erros);
    }

    @ExceptionHandler(BoletoNotValidException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public MensagemDeErroPersonalizado manipularBoletoNotValidException(BoletoNotValidException exception){
        List<ErroPersonalizado> erros = Arrays.asList(new ErroPersonalizado(exception.getMessage()));

        return new MensagemDeErroPersonalizado(422, erros);
    }

    @ExceptionHandler(ContaActivatedException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public MensagemDeErroPersonalizado manipularContaActivatedException(ContaActivatedException exception){
        List<ErroPersonalizado> erros = Arrays.asList(new ErroPersonalizado(exception.getMessage()));

        return new MensagemDeErroPersonalizado(422, erros);
    }

    @ExceptionHandler(ContaDeactivatedException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public MensagemDeErroPersonalizado manipularContaDeactivatedException(ContaDeactivatedException exception){
        List<ErroPersonalizado> erros = Arrays.asList(new ErroPersonalizado(exception.getMessage()));

        return new MensagemDeErroPersonalizado(422, erros);
    }

    @ExceptionHandler(ContaDuplicatedException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public MensagemDeErroPersonalizado manipularContaDuplicatedException(ContaDuplicatedException exception){
        List<ErroPersonalizado> erros = Arrays.asList(new ErroPersonalizado(exception.getMessage()));

        return new MensagemDeErroPersonalizado(422, erros);
    }

    @ExceptionHandler(ContaNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public MensagemDeErroPersonalizado manipularContaNotFoundException(ContaNotFoundException exception){
        List<ErroPersonalizado> erros = Arrays.asList(new ErroPersonalizado(exception.getMessage()));

        return new MensagemDeErroPersonalizado(404, erros);
    }

    @ExceptionHandler(ContaNotValidException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public MensagemDeErroPersonalizado manipularContaNotValidException(ContaNotValidException exception){
        List<ErroPersonalizado> erros = Arrays.asList(new ErroPersonalizado(exception.getMessage()));

        return new MensagemDeErroPersonalizado(422, erros);
    }

    @ExceptionHandler(EnderecoNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public MensagemDeErroPersonalizado manipularEnderecoNotFoundException(EnderecoNotFoundException exception){
        List<ErroPersonalizado> erros = Arrays.asList(new ErroPersonalizado(exception.getMessage()));

        return new MensagemDeErroPersonalizado(404, erros);
    }

    @ExceptionHandler(SaldoExistedException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public MensagemDeErroPersonalizado manipularSaldoExistedException(SaldoExistedException exception){
        List<ErroPersonalizado> erros = Arrays.asList(new ErroPersonalizado(exception.getMessage()));

        return new MensagemDeErroPersonalizado(422, erros);
    }

    @ExceptionHandler(SaldoNotEnoughException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public MensagemDeErroPersonalizado manipularSaldoNotEnoughException(SaldoNotEnoughException exception){
        List<ErroPersonalizado> erros = Arrays.asList(new ErroPersonalizado(exception.getMessage()));

        return new MensagemDeErroPersonalizado(422, erros);
    }

    @ExceptionHandler(UsuarioDuplicatedException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public MensagemDeErroPersonalizado manipularUsuarioDuplicatedException(UsuarioDuplicatedException exception){
        List<ErroPersonalizado> erros = Arrays.asList(new ErroPersonalizado(exception.getMessage()));

        return new MensagemDeErroPersonalizado(422, erros);
    }

    @ExceptionHandler(UsuarioNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public MensagemDeErroPersonalizado manipularUsuarioNotFoundException(UsuarioNotFoundException exception){
        List<ErroPersonalizado> erros = Arrays.asList(new ErroPersonalizado(exception.getMessage()));

        return new MensagemDeErroPersonalizado(404, erros);
    }

    @ExceptionHandler(OptionNotValidException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public MensagemDeErroPersonalizado manipularOptionNotValidException(OptionNotValidException exception){
        List<ErroPersonalizado> erros = Arrays.asList(new ErroPersonalizado(exception.getMessage()));

        return new MensagemDeErroPersonalizado(422, erros);
    }

    @ExceptionHandler(DataNotValidException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public MensagemDeErroPersonalizado manipularDataNotValidException(DataNotValidException exception){
        List<ErroPersonalizado> erros = Arrays.asList(new ErroPersonalizado(exception.getMessage()));

        return new MensagemDeErroPersonalizado(422, erros);
    }

}
