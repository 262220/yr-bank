package br.com.rede.Yr.Bank.conta;

import br.com.rede.Yr.Bank.operacao.Operacao;
import br.com.rede.Yr.Bank.usuario.Usuario;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "contas")
public class Conta {
    @Column(length = 7, nullable = false)
    private String agencia;
    @Id
    private String numConta;
    @Column(length = 1, nullable = false)
    private int digito;
    @OneToOne(cascade = CascadeType.ALL)
    private Usuario titular;
    @Column(nullable = false)
    private double saldo;
    @Column(nullable = false)
    private boolean ativa;
    @OneToMany(mappedBy = "id", cascade = CascadeType.ALL)
    private List<Operacao> transacoes;

    public Conta() {
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public String getNumConta() {
        return numConta;
    }

    public void setNumConta(String numConta) {
        this.numConta = numConta;
    }

    public int getDigito() {
        return digito;
    }

    public void setDigito(int digito) {
        this.digito = digito;
    }

    public Usuario getTitular() {
        return titular;
    }

    public void setTitular(Usuario titular) {
        this.titular = titular;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public boolean isAtiva() {
        return ativa;
    }

    public void setAtiva(boolean ativa) {
        this.ativa = ativa;
    }
}

