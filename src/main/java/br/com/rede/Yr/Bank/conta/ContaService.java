package br.com.rede.Yr.Bank.conta;

import br.com.rede.Yr.Bank.enuns.TipoTransacao;
import br.com.rede.Yr.Bank.exceptions.*;
import br.com.rede.Yr.Bank.operacao.Operacao;
import br.com.rede.Yr.Bank.operacao.OperacaoRepository;
import br.com.rede.Yr.Bank.operacao.OperacaoService;
import br.com.rede.Yr.Bank.usuario.Usuario;
import br.com.rede.Yr.Bank.usuario.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

@Service
public class ContaService {
    private ContaRepository contaRepository;
    private UsuarioService usuarioService;
    private OperacaoRepository operacaoRepository;

    @Autowired
    public ContaService(ContaRepository contaRepository, UsuarioService usuarioService, OperacaoRepository operacaoRepository) {
        this.contaRepository = contaRepository;
        this.usuarioService = usuarioService;
        this.operacaoRepository = operacaoRepository;
    }

    public Conta criarConta(String emailTitular) {
        Conta conta = new Conta();
        Usuario usuario = usuarioService.buscarUsuarioPorEmail(emailTitular);

        if (usuarioService.possuiConta(usuario.getCpf())) {
            conta.setAgencia("0001");
            conta.setDigito(7);
            conta.setTitular(usuario);
            conta.setSaldo(0.00);
            conta.setAtiva(true);

            long qtdContas = quantidadeContas();
            gerarNumConta(qtdContas, conta);

            usuario.setConta(conta);
        }

        return contaRepository.save(conta);
    }

    public void gerarNumConta(long qtdContas, Conta conta) {
        if (qtdContas == 0) {
            conta.setNumConta("00001");
        } else {
            String numConta = String.format("%05d", qtdContas + 1);
            conta.setNumConta(numConta);
        }
    }

    public long quantidadeContas() {
        List<Conta> contas = (List<Conta>) contaRepository.findAll();

        return contas.size();
    }

    public Conta buscarContaPorEmail(String email) {
        Usuario usuario = usuarioService.buscarUsuarioPorEmail(email);

        usuarioService.possuiConta(usuario);

        return usuario.getConta();
    }

    public Conta buscarContaPorNumero(String numConta) {
        Optional<Conta> contaOptional = contaRepository.findById(numConta);

        contaOptional.orElseThrow(() -> new ContaNotFoundException("Conta não existe!"));

        return contaOptional.get();
    }

    public double obterSaldo(String numConta) {
        return buscarContaPorNumero(numConta).getSaldo();
    }

    public Conta atualizarConta(Conta conta) {
        return contaRepository.save(conta);
    }

    public double sacar(String numConta, double valorSaque) {
        Conta conta = buscarContaPorNumero(numConta);
        usuarioService.possuiConta(conta.getTitular());
        double novoSaldo = conta.getSaldo() - valorSaque;

        if (contaAtiva(conta) && saldoSuficiente(valorSaque, conta.getSaldo())) {
            conta.setSaldo(novoSaldo);
            contaRepository.save(conta);
        }

        Operacao registrarOperacao = new Operacao();
        registrarOperacao.setDataOperacao(LocalDate.now());
        registrarOperacao.setHoraOperacao(LocalTime.now());
        registrarOperacao.setOrigem(conta);
        registrarOperacao.setValor(valorSaque);
        registrarOperacao.setTipoTransacao(TipoTransacao.NEGATIVA);

        operacaoRepository.save(registrarOperacao);

        return novoSaldo;
    }

    public boolean saldoSuficiente(double valorSaque, double saldoAtual) {
        if (valorSaque > saldoAtual) {
            throw new SaldoNotEnoughException("Saldo insuficiente!");
        } else {
            return true;
        }
    }

    public boolean contaAtiva(Conta conta) {
        if (!conta.isAtiva()) {
            throw new ContaDeactivatedException("Sua conta está desativada!");
        } else {
            return true;
        }
    }

    public boolean contaAtiva(Conta contaOrigem, Conta contaDestino) {
        if (!contaDestino.isAtiva()) {
            throw new ContaDeactivatedException("A conta destino está desativada!");
        }

        if (!contaOrigem.isAtiva()) {
            throw new ContaDeactivatedException("Sua conta está desativada!");
        }

        return true;
    }

    public void desativarConta(String cpfTitular) {
        Usuario titularConta = usuarioService.buscarUsuarioPorCpf(cpfTitular);
        Conta conta = titularConta.getConta();

        if (contaAtiva(conta) && saldoZerado(conta.getSaldo())) {
            conta.setAtiva(false);
        }

        contaRepository.save(conta);
    }

    public boolean saldoZerado(double saldo) {
        if (saldo != 0) {
            throw new SaldoExistedException("Retire seu  saldo para poder desativar sua conta !");
        } else {
            return true;
        }
    }

    public boolean contaDesativada(Conta conta) {
        if (conta.isAtiva()) {
            throw new ContaActivatedException("Sua Conta já está ativada!");
        } else {
            return true;
        }
    }

    public void ativarConta(String cpfTitular) {
        Usuario titularConta = usuarioService.buscarUsuarioPorCpf(cpfTitular);
        Conta conta = titularConta.getConta();

        if (contaDesativada(conta)) {
            conta.setAtiva(true);
        }

        contaRepository.save(conta);
    }

    public void verificaConta(String agencia, String numConta, int digito) {
        Conta conta = buscarContaPorNumero(numConta);

        if (!conta.getAgencia().equals(agencia) || conta.getDigito() != digito) {
            throw new ContaNotFoundException("Conta de destino não encontrada!");
        }
    }

    public void depositar(double valorDepositado, String agencia, String numConta, int digito) {
        Conta conta = buscarContaPorNumero(numConta);
        verificaConta(agencia, numConta, digito);

        double novoSaldo = conta.getSaldo() + valorDepositado;
        conta.setSaldo(novoSaldo);
        contaRepository.save(conta);

        Operacao operacao = new Operacao();
        operacao.setTipoTransacao(TipoTransacao.POSITIVA);
        operacao.setHoraOperacao(LocalTime.now());
        operacao.setDataOperacao(LocalDate.now());
        operacao.setValor(valorDepositado);
        operacao.setDestino(conta.getNumConta());
        operacaoRepository.save(operacao);
    }
}

