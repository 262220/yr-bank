package br.com.rede.Yr.Bank.conta;

import br.com.rede.Yr.Bank.conta.dtos.ContaDto;
import br.com.rede.Yr.Bank.conta.dtos.ObterSaldoDTO;
import br.com.rede.Yr.Bank.conta.dtos.OperacaoContaDto;
import br.com.rede.Yr.Bank.conta.dtos.SaqueDto;
import br.com.rede.Yr.Bank.exceptions.OptionNotValidException;
import br.com.rede.Yr.Bank.security.jwt.ComponenteJWT;
import io.jsonwebtoken.Claims;
import br.com.rede.Yr.Bank.conta.dtos.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.LocalDateTime;

@RestController
@RequestMapping("/conta")
public class ContaController {
    private ContaService contaService;
    private ModelMapper modelMapper;
    private ComponenteJWT componenteJWT;

    @Autowired
    public ContaController(ContaService contaService, ModelMapper modelMapper, ComponenteJWT componenteJWT) {
        this.contaService = contaService;
        this.modelMapper = modelMapper;
        this.componenteJWT = componenteJWT;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ContaDto criarConta(Authentication authentication) {
        String emailTitular = authentication.getName();

        return modelMapper.map(contaService.criarConta(emailTitular), ContaDto.class);
    }

    @GetMapping("/saldo")
    public ObterSaldoDTO obterSaldo(Authentication authentication) {
        String numConta = contaService.buscarContaPorEmail(authentication.getName()).getNumConta();

        return new ObterSaldoDTO(contaService.obterSaldo(numConta));
    }

    @PostMapping("/saque")
    public SaqueDto sacar(@RequestBody OperacaoContaDto operacaoContaDto, Authentication authentication) {
        String numConta = contaService.buscarContaPorEmail(authentication.getName()).getNumConta();

        return new SaqueDto(operacaoContaDto.getValor(), contaService.sacar(numConta, operacaoContaDto.getValor()), LocalDateTime.now());
    }

    @PutMapping
    public void ativarEDesativarConta(HttpServletRequest request, @RequestParam(required = false) boolean ativar, @RequestParam(required = false) boolean desativar) {
        String token = request.getHeader("Authorization");
        Claims claims = componenteJWT.getClaims(token.substring(6));

        if (ativar) {
            contaService.ativarConta(claims.getSubject());
        } else if (desativar) {
            contaService.desativarConta(claims.getSubject());
        } else {
            throw new OptionNotValidException("Opção inválida!");
        }
    }

    @PostMapping("/deposito")
    public RespostaDepositoDto depositar(@RequestBody @Valid DepositoDto depositoDto) {
        contaService.depositar(depositoDto.getValor(), depositoDto.getAgencia(), depositoDto.getNumConta(), depositoDto.getDigito());

        return new RespostaDepositoDto(depositoDto.getValor(), LocalDateTime.now());
    }
}

