package br.com.rede.Yr.Bank.security.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class ComponenteJWT {
    @Value("${jwt.chave}")
    private String chave;
    @Value("${jwt.milissegundos}")
    private Long milissegundos;

    public String gerarToken(String cpf, String email){
        Date vecimento = new Date(System.currentTimeMillis() + milissegundos);

        String token = Jwts.builder().setSubject(cpf).claim("email", email).setExpiration(vecimento)
                .signWith(SignatureAlgorithm.HS512, chave.getBytes()).compact();

        return token;
    }

    public Claims getClaims(String token){
        try {
            Claims claims = Jwts.parser().setSigningKey(chave.getBytes()).parseClaimsJws(token).getBody();
            return claims;
        }catch (Exception e){
            throw new RuntimeException("Token Inválido!");
        }
    }

    public boolean isTokenValid(String token){
        try {
            Claims claims = getClaims(token);
            String cpf = claims.getSubject();
            Date vencimento = claims.getExpiration();
            Date dataAtual = new Date(System.currentTimeMillis());

            return cpf != null && vencimento != null && dataAtual.before(vencimento);
        }catch (RuntimeException e){
            return false;
        }
    }

}
