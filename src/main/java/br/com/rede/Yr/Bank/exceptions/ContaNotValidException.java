package br.com.rede.Yr.Bank.exceptions;

public class ContaNotValidException extends RuntimeException {
    public ContaNotValidException(String message) {
        super(message);
    }
}
