package br.com.rede.Yr.Bank.conta.dtos;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class SaqueDto {
    private double valorSacado;
    private double saldoAtual;
    private LocalDateTime dataSaque;

    public SaqueDto(double valorSacado, double saldoAtual, LocalDateTime dataSaque) {
        this.valorSacado = valorSacado;
        this.saldoAtual = saldoAtual;
        this.dataSaque = dataSaque;
    }

    public SaqueDto() {
    }

    public double getSaldoAtual() {
        return saldoAtual;
    }

    public void setSaldoAtual(double saldoAtual) {
        this.saldoAtual = saldoAtual;
    }

    public String getDataSaque() {
        return formatar(dataSaque);
    }

    public void setDataSaque(LocalDateTime dataSaque) {
        this.dataSaque = dataSaque;
    }

    public double getValorSacado() {
        return valorSacado;
    }

    public void setValorSacado(double valorSacado) {
        this.valorSacado = valorSacado;
    }

    public String formatar(LocalDateTime localDate){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        return localDate.format(formatter);
    }

}
