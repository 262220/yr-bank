package br.com.rede.Yr.Bank.exceptions;

public class SaldoNotEnoughException extends RuntimeException {
    public SaldoNotEnoughException(String message) {
        super(message);
    }
}
