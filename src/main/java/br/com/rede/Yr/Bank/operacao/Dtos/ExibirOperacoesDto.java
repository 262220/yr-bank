package br.com.rede.Yr.Bank.operacao.Dtos;

import br.com.rede.Yr.Bank.conta.dtos.ContaDto;
import br.com.rede.Yr.Bank.enuns.TipoTransacao;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class ExibirOperacoesDto {
    private LocalDate dataOperacao;
    private String destino;
    private TipoTransacao tipoTransacao;
    private double valor;
    private ContaDto origem;

    public ExibirOperacoesDto(LocalDate dataOperacao, String destino, TipoTransacao tipoTransacao, double valor, ContaDto origem) {
        this.dataOperacao = dataOperacao;
        this.destino = destino;
        this.tipoTransacao = tipoTransacao;
        this.valor = valor;
        this.origem = origem;
    }

    public ExibirOperacoesDto() {
    }

    public String getDataOperacao() {
        return formatar(dataOperacao);
    }

    public void setDataOperacao(LocalDate dataOperacao) {
        this.dataOperacao = dataOperacao;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public TipoTransacao getTipoTransacao() {
        return tipoTransacao;
    }

    public void setTipoTransacao(TipoTransacao tipoTransacao) {
        this.tipoTransacao = tipoTransacao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public ContaDto getOrigem() {
        return origem;
    }

    public void setOrigem(ContaDto origem) {
        this.origem = origem;
    }

    public String formatar(LocalDate localDate){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return localDate.format(formatter);
    }

}
