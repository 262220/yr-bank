package br.com.rede.Yr.Bank.endereco;

import org.springframework.data.repository.CrudRepository;

public interface EnderecoRepository extends CrudRepository<Endereco, Integer> {

}
