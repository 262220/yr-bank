package br.com.rede.Yr.Bank.exceptions;

public class ContaDuplicatedException extends RuntimeException {
    public ContaDuplicatedException(String message) {
        super(message);
    }
}
