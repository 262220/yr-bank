package br.com.rede.Yr.Bank.usuario;

import br.com.rede.Yr.Bank.endereco.Endereco;
import br.com.rede.Yr.Bank.endereco.EnderecoService;
import br.com.rede.Yr.Bank.exceptions.ContaDuplicatedException;
import br.com.rede.Yr.Bank.exceptions.ContaNotFoundException;
import br.com.rede.Yr.Bank.exceptions.UsuarioDuplicatedException;
import br.com.rede.Yr.Bank.exceptions.UsuarioNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioService {
    private UsuarioRepository usuarioRepository;
    private EnderecoService enderecoService;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UsuarioService(UsuarioRepository usuarioRepository, EnderecoService enderecoService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.usuarioRepository = usuarioRepository;
        this.enderecoService = enderecoService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public void cadastrarUsuario(Usuario usuario) {
        usuario.setSenha(bCryptPasswordEncoder.encode(usuario.getSenha()));
        usuario.getEndereco().setMorador(usuario);
        verficarUsuarioExistente(usuario);
        usuarioRepository.save(usuario);
    }

    public Usuario buscarUsuarioPorCpf(String cpf) {
        Optional<Usuario> usuarioOptional = usuarioRepository.findById(cpf);

        usuarioOptional.orElseThrow(() -> new UsuarioNotFoundException("Usuário não encontrado!"));

        return usuarioOptional.get();
    }

    public Usuario buscarUsuarioPorEmail(String email) {
        Optional<Usuario> usuarioOptional = usuarioRepository.findByEmailContains(email);

        usuarioOptional.orElseThrow(() -> new UsuarioNotFoundException("Usuário não encontrado!"));

        return usuarioOptional.get();
    }

    public void atualizarUsuario(Usuario usuarioDTO, String email) {
        Usuario usuario = buscarUsuarioPorEmail(email);
        usuario.setNome(usuarioDTO.getNome());
        usuario.setEmail(usuarioDTO.getEmail());
        usuario.setTelefone(usuarioDTO.getTelefone());
        Endereco endereco = enderecoService.atualizarEndereco(usuario.getEndereco().getId(), usuarioDTO.getEndereco());
        usuario.setEndereco(endereco);
        usuarioRepository.save(usuario);
    }

    public void verficarUsuarioExistente(Usuario usuario) {
        if (usuarioRepository.existsById(usuario.getCpf())) {
            throw new UsuarioDuplicatedException("Usuário já cadastrado!");
        }
    }

    public boolean possuiConta(String cpf) {
        Usuario usuario = buscarUsuarioPorCpf(cpf);

        if (usuario.getConta() != null) {
            throw new ContaDuplicatedException("Você já possui uma conta!");
        } else {
            return true;
        }
    }

    public boolean possuiConta(Usuario usuario) {
        if (usuario.getConta() == null) {
            throw new ContaNotFoundException("Você ainda não possui uma conta!");
        }
        return true;
    }

}

