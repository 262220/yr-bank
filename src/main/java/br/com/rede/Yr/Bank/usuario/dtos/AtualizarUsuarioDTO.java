package br.com.rede.Yr.Bank.usuario.dtos;

import br.com.rede.Yr.Bank.endereco.dtos.EnderecoDto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class AtualizarUsuarioDTO {
    @Size( min = 2, message = "{validacao.nome}")
    @NotBlank(message = "{validacao.nome.obrigatorio}")
    private String nome;
    @Email(message = "{validacao.email}")
    @NotBlank(message = "{validacao.email.obrigatorio}")
    private String email;
    @Size( min = 10, max = 11, message = "{validacao.telefone}")
    private String telefone;

    private EnderecoDto endereco;

    public AtualizarUsuarioDTO() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public EnderecoDto getEndereco() {
        return endereco;
    }

    public void setEndereco(EnderecoDto endereco) {
        this.endereco = endereco;
    }
}
