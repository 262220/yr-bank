package br.com.rede.Yr.Bank.exceptions;

public class BoletoNotValidException extends RuntimeException {
    public BoletoNotValidException(String message) {
        super(message);
    }
}
