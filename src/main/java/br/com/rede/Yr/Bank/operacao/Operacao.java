package br.com.rede.Yr.Bank.operacao;

import br.com.rede.Yr.Bank.conta.Conta;
import br.com.rede.Yr.Bank.enuns.TipoTransacao;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;


@Entity
@Table(name = "operacoes")
public class Operacao {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Enumerated(EnumType.STRING)
    private TipoTransacao tipoTransacao;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "conta_origem", nullable = true)
    private Conta origem;

    private String destino;
    @Column(nullable = false)
    private LocalDate dataOperacao;
    @Column(nullable = false)
    private LocalTime horaOperacao;
    @Column(nullable = false)
    private double valor;

    public Operacao() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TipoTransacao getTipoTransacao() {
        return tipoTransacao;
    }

    public void setTipoTransacao(TipoTransacao tipoTransacao) {
        this.tipoTransacao = tipoTransacao;
    }

    public Conta getOrigem() {
        return origem;
    }

    public void setOrigem(Conta origem) {
        this.origem = origem;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public LocalDate getDataOperacao() {
        return dataOperacao;
    }

    public void setDataOperacao(LocalDate dataOperacao) {
        this.dataOperacao = dataOperacao;
    }

    public LocalTime getHoraOperacao() {
        return horaOperacao;
    }

    public void setHoraOperacao(LocalTime horaOperacao) {
        this.horaOperacao = horaOperacao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
