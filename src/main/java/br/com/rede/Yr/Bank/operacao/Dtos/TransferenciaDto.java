package br.com.rede.Yr.Bank.operacao.Dtos;

import br.com.rede.Yr.Bank.conta.Conta;

import javax.validation.constraints.*;

public class TransferenciaDto {
    @NotBlank(message = "{validacao.agencia}")
    @Size(min = 4,max = 4, message = "{validacao.agencia.min}")
    private String agencia;
    @NotBlank(message = "{validacao.numConta}")
    @Size(min = 5, max = 5, message = "{validacao.conta.min}")
    private String numConta;
    @NotNull(message = "{validacao.digito}")
    @Min(value = 1, message = "{validacao.digito.valor.menor}")
    @Max(value = 7, message = "{validacao.digito.valor.maior}")
    private int digito;
    @NotNull(message = "{validacao.valor}")
    @Min(value = 1, message = "{validacao.valor.min}")
    private double valor;


    public TransferenciaDto() {
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public String getNumConta() {
        return numConta;
    }

    public void setNumConta(String numConta) {
        this.numConta = numConta;
    }

    public int getDigito() {
        return digito;
    }

    public void setDigito(int digito) {
        this.digito = digito;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
