package br.com.rede.Yr.Bank.security;

import br.com.rede.Yr.Bank.security.jwt.ComponenteJWT;
import io.jsonwebtoken.Claims;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AuthorizationFilter extends BasicAuthenticationFilter {
    private ComponenteJWT componenteJWT;
    private UserDetailsService userDetailsService;

    public AuthorizationFilter(AuthenticationManager authenticationManager, ComponenteJWT componenteJWT, UserDetailsService userDetailsService) {
        super(authenticationManager);
        this.componenteJWT = componenteJWT;
        this.userDetailsService = userDetailsService;
    }

    public UsernamePasswordAuthenticationToken pegarAutenticacao(HttpServletRequest request, String token){
        if(!componenteJWT.isTokenValid(token)){
            throw new RuntimeException("Token inválido!");
        }

        Claims claims = componenteJWT.getClaims(token);
        UserDetails usuario = userDetailsService.loadUserByUsername((String) claims.get("email"));

        return new UsernamePasswordAuthenticationToken(usuario,null,usuario.getAuthorities());

    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String token = request.getHeader("Authorization");

        if(token != null && token.startsWith("Token ")){
            try {
                UsernamePasswordAuthenticationToken auth = pegarAutenticacao(request, token.substring(6));
                SecurityContextHolder.getContext().setAuthentication(auth);
            }catch (RuntimeException e){
                System.out.println(e.getMessage());
            }
        }
        chain.doFilter(request,response);
    }

}
