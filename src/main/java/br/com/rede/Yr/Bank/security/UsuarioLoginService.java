package br.com.rede.Yr.Bank.security;

import br.com.rede.Yr.Bank.usuario.Usuario;
import br.com.rede.Yr.Bank.usuario.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioLoginService implements UserDetailsService {
    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<Usuario> usuarioOptional = usuarioRepository.findByEmailContains(email);

        usuarioOptional.orElseThrow(() -> new UsernameNotFoundException("Usuario não encontrado"));

        Usuario usuario = usuarioOptional.get();
        return new UsuarioLogin(usuario.getCpf(), usuario.getEmail(), usuario.getSenha());
    }
}
