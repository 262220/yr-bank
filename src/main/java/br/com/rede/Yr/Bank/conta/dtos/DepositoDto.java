package br.com.rede.Yr.Bank.conta.dtos;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

public class DepositoDto {
    @Size(min = 4, max = 4, message = "{validacao.agencia.min}")
    private String agencia;
    @Size(min = 5, max = 5, message = "{validacao.conta.min}")
    private String numConta;
    @Min(value = 1, message = "{validacao.digito.valor.menor}")
    @Max(value = 9, message = "{validacao.digito.valor.maior}")
    private int digito;
    @Min(value = 1, message = "{validacao.conta}")
    private double valor;

    public DepositoDto() {
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public String getNumConta() {
        return numConta;
    }

    public void setNumConta(String numConta) {
        this.numConta = numConta;
    }

    public int getDigito() {
        return digito;
    }

    public void setDigito(int digito) {
        this.digito = digito;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
