package br.com.rede.Yr.Bank.operacao.Dtos;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DataOperacaoDto {
    private String dataOperacao;

    public DataOperacaoDto() {
    }

    public String getDataOperacao() {
        return dataOperacao;
    }

    public void setDataOperacao(String dataOperacao) {
        this.dataOperacao = dataOperacao;
    }

}
