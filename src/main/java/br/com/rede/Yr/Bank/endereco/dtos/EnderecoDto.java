package br.com.rede.Yr.Bank.endereco.dtos;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class EnderecoDto {

@NotBlank(message = "{validacao.estado.obrigatorio}")
private String estado;
@NotBlank(message = "{validacao.cidade.obrigatorio}")
private String cidade;
@NotBlank(message = "{validacao.bairro.obrigatorio}")
private String bairro;
@NotBlank(message = "{validacao.cep.obrigatorio}")
@Size( min = 8, max = 8, message = "{validacao.cep.tamanho}")
private String cep;
@NotBlank(message = "{validacao.logradouro.obrigatorio}")
private String logradouro;
@Min(value = 1, message = "{validacao.numero}")
private int numero;
private String complemento;

    public EnderecoDto() {
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }
}
