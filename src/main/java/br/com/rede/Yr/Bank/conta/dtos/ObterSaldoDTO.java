package br.com.rede.Yr.Bank.conta.dtos;

public class ObterSaldoDTO {
    private double saldo;

    public ObterSaldoDTO() {
    }

    public ObterSaldoDTO(double saldo) {
        this.saldo = saldo;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }
}
