package br.com.rede.Yr.Bank.exceptions;

public class UsuarioDuplicatedException extends RuntimeException {
    public UsuarioDuplicatedException(String message) {
        super(message);
    }
}
