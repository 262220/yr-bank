package br.com.rede.Yr.Bank.operacao.Dtos;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class RespostaTransferenciaDTO {
    private String nome;
    private String cpf;
    private String agencia;
    private String numConta;
    private int digito;
    private double valor;
    private LocalDateTime dataTransferencia;

    public RespostaTransferenciaDTO() {
    }

    public RespostaTransferenciaDTO(String nome, String cpf, String agencia, String numConta, int digito, double valor, LocalDateTime dataTransferencia) {
        this.nome = nome;
        this.cpf = cpf;
        this.agencia = agencia;
        this.numConta = numConta;
        this.digito = digito;
        this.valor = valor;
        this.dataTransferencia = dataTransferencia;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public String getNumConta() {
        return numConta;
    }

    public void setNumConta(String numConta) {
        this.numConta = numConta;
    }

    public int getDigito() {
        return digito;
    }

    public void setDigito(int digito) {
        this.digito = digito;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getDataTransferencia() {
        return formatar(dataTransferencia);
    }

    public void setDataTransferencia(LocalDateTime dataTransferencia) {
        this.dataTransferencia = dataTransferencia;
    }

    public String formatar(LocalDateTime dateTime){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        return dateTime.format(formatter);
    }

}
