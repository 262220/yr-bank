package br.com.rede.Yr.Bank.exceptions;

public class EnderecoNotFoundException extends RuntimeException {
    public EnderecoNotFoundException(String message) {
        super(message);
    }
}
