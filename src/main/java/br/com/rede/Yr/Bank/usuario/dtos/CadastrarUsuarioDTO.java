package br.com.rede.Yr.Bank.usuario.dtos;

import br.com.rede.Yr.Bank.conta.Conta;
import br.com.rede.Yr.Bank.endereco.Endereco;
import br.com.rede.Yr.Bank.endereco.dtos.EnderecoDto;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;



public class CadastrarUsuarioDTO {
    @Size( min = 2, message = "{validacao.nome}")
    @NotBlank(message = "{validacao.nome.obrigatorio}")
    private String nome;
    @Size(min = 11, max = 11, message = "{validacao.cpf}")
    @NotBlank(message = "{validacao.cpf.obrigatorio}")
    private String cpf;
    @Email(message = "{validacao.email}")
    @NotBlank(message = "{validacao.email.obrigatorio}")
    private String email;
    @Size( min = 10, max = 11, message = "{validacao.telefone}")
    private String telefone;
    @NotBlank(message = "{validacao.senha.obrigatorio}")
    private String senha;
    @Valid
    @NotNull
    private EnderecoDto endereco;


    public CadastrarUsuarioDTO() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public EnderecoDto getEndereco() {
        return endereco;
    }

    public void setEndereco(EnderecoDto endereco) {
        this.endereco = endereco;
    }
}
