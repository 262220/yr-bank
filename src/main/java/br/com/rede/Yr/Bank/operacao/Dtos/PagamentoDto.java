package br.com.rede.Yr.Bank.operacao.Dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class PagamentoDto {
    @NotBlank(message = "{validacao.boleto}")
    @Size(min = 47,max = 48, message = "{validacao.numero.boleto}")
    private String codBarras;

    public PagamentoDto() {
    }

    public String getCodBarras() {
        return codBarras;
    }

    public void setCodBarras(String codBarras) {
        this.codBarras = codBarras;
    }
}
