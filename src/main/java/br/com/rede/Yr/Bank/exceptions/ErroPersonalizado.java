package br.com.rede.Yr.Bank.exceptions;

public class ErroPersonalizado {
    private String mensagem;

    public ErroPersonalizado(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
}
