package br.com.rede.Yr.Bank.conta;

import org.springframework.data.repository.CrudRepository;

public interface ContaRepository extends CrudRepository<Conta, String> {
}
