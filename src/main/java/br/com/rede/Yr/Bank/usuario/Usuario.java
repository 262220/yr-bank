// versão 1.2 - Sthefam
package br.com.rede.Yr.Bank.usuario;


import br.com.rede.Yr.Bank.conta.Conta;
import br.com.rede.Yr.Bank.endereco.Endereco;

import javax.persistence.*;

@Entity
@Table(name = "usuarios")
public class Usuario {
    @Column(length = 100,nullable = false)
    private String nome;
    @Id
    private String cpf;
    @Column(length = 50,unique = true, nullable = false)
    private String email;
    @Column(length = 13)
    private String telefone;
    @Column(nullable = false)
    private String senha;
    @OneToOne(cascade = CascadeType.ALL)
    private Endereco endereco;
    @OneToOne(cascade = CascadeType.ALL)
    private Conta conta;

    public Usuario() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public Conta getConta() {
        return conta;
    }

    public void setConta(Conta conta) {
        this.conta = conta;
    }
}
