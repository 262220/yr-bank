package br.com.rede.Yr.Bank.exceptions;

public class ContaNotFoundException extends RuntimeException{
    public ContaNotFoundException(String message) {
        super(message);
    }
}
