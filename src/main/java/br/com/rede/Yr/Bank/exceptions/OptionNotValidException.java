package br.com.rede.Yr.Bank.exceptions;

public class OptionNotValidException extends RuntimeException {
    public OptionNotValidException(String message) {
        super(message);
    }
}
