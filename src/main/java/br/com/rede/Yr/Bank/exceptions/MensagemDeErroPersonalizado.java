package br.com.rede.Yr.Bank.exceptions;

import java.util.List;

public class MensagemDeErroPersonalizado {

    private int statusCode;
    private List<ErroPersonalizado> erros;

    public MensagemDeErroPersonalizado(int statusCode, List<ErroPersonalizado> erros) {
        this.statusCode = statusCode;
        this.erros = erros;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public List<ErroPersonalizado> getErros() {
        return erros;
    }

    public void setErros(List<ErroPersonalizado> erros) {
        this.erros = erros;
    }

}
